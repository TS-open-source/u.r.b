import { darken, lighten, readableColor } from 'polished';
import styled from 'styled-components';
import { Props } from '.';


export const Container = styled.button<Props>`
  background-color: ${(props) =>
    props.outlined ? 'transparent' : props.backgroundColor};
  color: ${({ color }: Props) =>
    readableColor(color as string, '#fff', '#19181f')};
  padding: 12px 20px;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 20px;
  border: ${(props) =>
    props.outlined ? `1px solid ${props.backgroundColor}` : 'none'};
  cursor: pointer;
  &:disabled {
    opacity: 0.7;
    cursor: not-allowed;
  }
  &:hover:not(:disabled) {
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-color: transparent;
    background-color: ${(props) =>
      props.outlined
        ? props.backgroundColor
        : lighten(0.03, props.backgroundColor as string)};
  }
  :active:not(:disabled) {
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    background-color: ${(props) =>
      darken(0.03, props.backgroundColor as string)};
  }
`;