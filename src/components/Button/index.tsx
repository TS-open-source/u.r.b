import React from 'react';
import { Container } from './styles';


export interface Props {
  backgroundColor?: string;
  color?: string;
  outlined?: boolean;
  onClick?: () => void;
  disabled?: boolean;
  children: React.ReactNode | string;
}

const ButtonWrapper: React.FC<Props> = ({
  children,
  backgroundColor = '#32abf6',
  color = '#fff',
  outlined = false,
  ...props
}) => {
  return (
    <Container
      backgroundColor={backgroundColor}
      color={color}
      outlined={outlined}
      {...props}
    >
      {children}
    </Container>
  );
};

export { ButtonWrapper as Button };
