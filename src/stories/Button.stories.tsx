import { storiesOf } from '@storybook/react';
import React from 'react';
import { Button } from '../components/Button';

storiesOf('Button', module)
  .add('Default', () => <Button>Default</Button>)
  .add('Outlined', () => <Button outlined>Outlined</Button>)
  .add('Disabled', () => <Button disabled>Default</Button>);