import { storiesOf } from '@storybook/react';
import React, { useState } from 'react';
import { Button } from '../components/Button';
import { Modal } from '../components/Modal';



storiesOf('Modal', module).add('Modal', () => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button onClick={() => setOpen(true)}>Open modal</Button>

      <Modal title="Título" open={open} setOpen={setOpen}>
        Aqui fica algum conteudo
      </Modal>
    </>
  );
});