import { storiesOf } from '@storybook/react';
import React, { useState } from 'react';
import { MdSearch } from 'react-icons/md';
import { Input } from '../components/Input';




storiesOf('Input', module)
  .add('Default', () => {
    const [inputValue, setInputValue] = useState('');

    return (
      <Input
        name="name"
        placeholder=""
        value={inputValue}
        setValue={setInputValue}
      />
    );
  })
  .add('With Icon', () => {
    const [inputValue, setInputValue] = useState('');

    return (
      <Input
        name="name"
        icon={<MdSearch />}
        placeholder=""
        value={inputValue}
        setValue={setInputValue}
      />
    );
  });